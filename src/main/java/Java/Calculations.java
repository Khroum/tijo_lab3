package Java;

public class Calculations {
    public static Point2D positionGeometricCenter(Point2D[] points){
        double sumX = 0;
        double sumY = 0;

        for(Point2D point:points){
            sumX+=point.getX();
            sumY+=point.getY();
        }

        return new Point2D(sumX/points.length,sumY/points.length);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints){
        double sumX = 0;
        double sumY = 0;
        double sumMass= 0;

        for(MaterialPoint2D point:materialPoints){
            sumX += point.getX()*point.getMass();
            sumY += point.getY()*point.getMass();
            sumMass += point.getMass();
        }

        return new MaterialPoint2D(sumX/sumMass,sumY/sumMass, sumMass);
    }
}
