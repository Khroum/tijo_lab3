package Kotlin

class MaterialPoint2D(x: Double, y: Double,public val mass:Double): Point2D(x, y) {
    override fun toString(): String {
        return "MaterialPoint2D(x=$x, y=$y, mass=$mass)"
    }
}