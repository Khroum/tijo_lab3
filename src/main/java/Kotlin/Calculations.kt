package Kotlin

object Calculations {


        @JvmStatic
        fun positionGeometricCenter(points:Array<Point2D>) : Point2D{
            var sumX=0.0;
            var sumY=0.0;
            points.forEach { point ->
                run {
                    sumX += point.x
                    sumY += point.y
                }
            }
    
            return Point2D(sumX/points.size, sumY/points.size)
        }

        @JvmStatic
        fun positionCenterOfMass(materialPoints:Array<MaterialPoint2D>) : Point2D{
            var sumX=0.0;
            var sumY=0.0;
            var sumMass=0.0;
            materialPoints.forEach { point ->
                run {
                    sumX += point.x*point.mass
                    sumY += point.y*point.mass
                    sumMass += point.mass
                }
            }
    
            return MaterialPoint2D(sumX/sumMass, sumY/sumMass, sumMass)
        }
    }
