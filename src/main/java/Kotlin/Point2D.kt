package Kotlin

open class Point2D(public val x:Double, public val y:Double) {
    override fun toString(): String {
        return "Point2D(x=$x, y=$y)"
    }
}