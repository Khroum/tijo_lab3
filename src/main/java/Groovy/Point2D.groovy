package Groovy

class Point2D {
    protected double x,y

    Point2D(double x, double y) {
        this.x = x
        this.y = y
    }

    @Override
    String toString() {
        "Point2D{" +
         "x=$x, " +
         "y=$y}"
    }
}