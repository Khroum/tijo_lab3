package Groovy

class Calculations {
    static Point2D positionGeometricCenter(Point2D[] points){
        double sumX = 0
        double sumY = 0

        for(Point2D point:points){
            sumX+=point.x
            sumY+=point.y
        }

        return new Point2D(sumX/points.length,sumY/points.length)
    }

    static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints){
        double sumX = 0
        double sumY = 0
        double sumMass= 0
        for(MaterialPoint2D point:materialPoints){
            sumX += point.x*point.mass
            sumY += point.y*point.mass
            sumMass += point.mass
        }

        return new MaterialPoint2D(sumX/sumMass,sumY/sumMass, sumMass)
    }
}