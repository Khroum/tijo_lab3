package Groovy

class MaterialPoint2D extends Point2D {
    private double mass

    MaterialPoint2D(double x, double y, double mass) {
        super(x, y)
        this.mass = mass
    }

    @Override
    String toString() {
        "MaterialPoint2D{" +
        "x=$x, "+
        "y=$y, "+
        "mass=$mass}"
    }
}
